# lexik/jwt-authentication-bundle

JWT authentication for Symfony REST API. [lexik/jwt-authentication-bundle](https://packagist.org/packages/lexik/jwt-authentication-bundle)

* [*JWT Authentication and Refresh Token to API Platform*
  ](https://medium.com/@biberogluyusuf/jwt-authentication-and-refresh-token-on-api-platform-7c6ecaa8e236)
  2020-08 Yusuf Biberoğlu
* [*[Angular + Symfony] JWT Authentication*
  ](https://medium.com/@liujijieseason/angular-symfony-jwt-authentication-5fd39da2a1ac)
  2020-07 Jijie Liu